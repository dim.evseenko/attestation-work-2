'use strict'

// Ура! Практически всё получилось

// переменные
const form = document.querySelector('.form')
const inputs = document.querySelectorAll('.input')
const checkbox = document.querySelectorAll('.checkbox')
const submitButton = document.querySelector('.submit-button')

const emptyMessage = 'Поле обязательно для заполнения'
const invalidEmail = 'Email невалидный'
const invalidPass = 'Пароль должен содержать минимум 8 символов'

const regexp = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu

let user = { name: '', pass: ''}

// проверяет пустой ли инпут после потери фокуса
const blurInput = (event) => {
  // console.log(event);
  if (event.target.value === '') {
    event.target.nextElementSibling.textContent = emptyMessage
    // выводим сообщение - Заполните поле
    event.target.classList.add('error-label')
    // красим бордер инпута в красный
  } else {
    event.target.nextElementSibling.textContent = ''
    // убираем сообщение - Заполните поле
    event.target.classList.remove('error-label')
    // снимаем выделение бордера инпута
  }
}

// проверяет значения на валидность
const validationForm = (event) => {
  if (!regexp.test(user.name)) {
    // условие отрабатывает если email невалиден
    event.srcElement[0].nextElementSibling.textContent = invalidEmail
    // выводим сообщение о том что он невалиден
    event.srcElement[0].classList.add('error-label')
    // красим бордер инпута в красный
    return false
  }

  if (user.pass.length < 8) {
    // условие отработает если длинна пароля меньше 8 символов
    event.srcElement[1].nextElementSibling.textContent = invalidPass
    // выводим сообщение о том что пароль короткий
    event.srcElement[1].classList.add('error-label')
    // красим бордер инпута в красный
    return false
  } else {
    event.srcElement[0].classList.remove('error-label')
    // скрываем красную линию обводки email
    event.srcElement[1].classList.remove('error-label')
    // скрываем красную линию обводки пароля
  }

  if (!event.srcElement[2].checked) {
    // условие отработает если чекбокс не выставлен
    event.srcElement[2].nextElementSibling.nextElementSibling.textContent = emptyMessage
    // выводит сообщение о том что чекбокс должен быть заполнен
    event.srcElement[2].classList.add('checkbox-error')
    // класс который добавляет стили ошибки для чекбокса
    return false
  }

  // только в том случае если ни одно из указанных выше условий не отработает функция возвращает true и форма отправляется
  return true
}

// Авторизация пользователя
const validationUser = (e) => {
  let usersArr = []
  // Получение данных из локального хранилища
  let newUser = {
    name: e.target[0].value,
    pass: e.target[1].value
  }
  console.log('newUser', newUser)

  if (localStorage.getItem("users") === null) {
    alert('пользователь не зарегестрирован')
  } else {
    usersArr = JSON.parse(localStorage.getItem("users"))
    // Проверка наличия пользователя с введенными данными
    usersArr.forEach((item) => {
      if (item.name === newUser.name) {
        // console.log('Такой пользователь есть')
        if (item.pass === newUser.pass) {
          window.location.replace('./GoodRegistration.html')
          // Перенаправление на страницу
        } else {
        e.srcElement[0].classList.add('error-label')
        // красим бордер инпута в красный
        e.srcElement[1].nextElementSibling.textContent = 'Логин или Пароль невереный'
        // выводим сообщение
        e.srcElement[1].classList.add('error-label')
        // красим бордер инпута в красный
        }
        return
      } 
      else if (item.name !== newUser.name) {
        e.srcElement[0].nextElementSibling.textContent = emptyMessage
        // выводим сообщение - Заполните поле
        e.srcElement[0].classList.add('error-label')
        // красим бордер инпута в красный
        e.srcElement[1].nextElementSibling.textContent = emptyMessage
        // выводим сообщение - Заполните поле
        e.srcElement[1].classList.add('error-label')
        // красим бордер инпута в красный
      }
    })
  }
}

// функция очищает инпуты и объект user после отправки формы
const clearInput = (event) => {
  // очищает значене в первом инпуте
  event.srcElement[0].value = ''

  // очищает значене во втором инпуте 
  event.srcElement[1].value = ''

  // делает значение чекбокса по умолчанию
  event.srcElement[2].checked = false

  // скрывает значение emptyMessage
  event.srcElement[2].nextElementSibling.nextElementSibling.textContent = ''

  // сбрасывает значение объекта user поумолчанию
  // user = { name: '', pass: ''}

  // заменяем #heading после отправки формы
  const heading = document.getElementById('heading')
  heading.innerHTML = 'Вход'

  // заменяем #text после отправки формы
  const text = document.getElementById('text')
  text.innerHTML = 'Зарегистрироваться'

  // заменяем #checkbox-info после отправки формы
  const checkbox = document.getElementById('checkbox-info')
  checkbox.innerHTML = 'Я согласен с <u>Правилами пользования приложением</u>'

  // заменяем #submit после отправки формы
  const submit = document.getElementById('submit')
  submit.innerHTML = 'Вход'

  event.srcElement[0].classList.add('input')
}

// функция отправки данных пользователя в localStorage
const sendUser = (obj) => {
  let usersArr = []
  // массив пользователей

  if (localStorage.getItem("users") === null) {
    // условие отработает если в сторадже нет уже зарегестрированных пользователей
    usersArr.push(obj)
    // добавляет пользователя в массив пользователей
    localStorage.setItem('users', JSON.stringify(usersArr))
    // переводит массив пользователей в формат JSON и сохраняет в localStorage
  } else {
    // отработает если пользователи в localStorage уже есть
    usersArr = JSON.parse(localStorage.getItem("users"))
    // достает из стораджа массив пользователей и переводит его из JSON формата в обычный вид
    usersArr.push(obj)
    // добавляем в массив пользователей нового пользователея
    localStorage.setItem('users', JSON.stringify(usersArr))
    // переводит массив пользователей в формат JSON и сохраняет в localStorage
  }
}

// повесили обработчики событий на все инпуты
inputs.forEach(input => {
  input.addEventListener('blur', blurInput)
  // событие blur срабатывает при потере инпутом фокуса
  input.addEventListener('input', (event) => {
    // console.log('event: ', event);
    if (event.target.id === 'email') {
      // если вводится текст в инпут email то сохраняем в полe name объекта user
      user.name = event.target.value
    } else if (event.target.id === 'password') {
      // если вводится текст в инпут password то сохраняем в полe name объекта user
      user.pass = event.target.value
    }
  })
})

// вешаем обработчик событий submit на форму
form.addEventListener('submit', (event) => {
  // Отменяем отправку формы
  event.preventDefault()
  // здесь у меня сломалась логика, т.к. часть этих строк лишняя, но всё работает
  if (submitButton.textContent === 'Вход') {
    validationUser(event)
  } else if (submitButton.textContent === 'Регистрация') {
    // отменяет поведение по умолчанию
    if (validationForm(event)) {
      sendUser(user)
      // после чего очищаем все значения
      clearInput(event)
    } else {
      // если форма невалидна то программа прекращает работу
      return
    }
  }
})